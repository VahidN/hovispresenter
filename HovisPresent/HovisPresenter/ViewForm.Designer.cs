﻿namespace HovisPresent
{
	partial class ViewForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewForm));
			this.nextPicture = new System.Windows.Forms.PictureBox();
			this.currentLabel = new System.Windows.Forms.Label();
			this.nextLabel = new System.Windows.Forms.Label();
			this.startedTime = new System.Windows.Forms.Timer(this.components);
			this.currentPicture = new System.Windows.Forms.PictureBox();
			this.netDisconnectedIcon = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this.nextPicture)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.currentPicture)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.netDisconnectedIcon)).BeginInit();
			this.SuspendLayout();
			// 
			// nextPicture
			// 
			this.nextPicture.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.nextPicture.BackColor = System.Drawing.Color.Black;
			this.nextPicture.Location = new System.Drawing.Point(500, 207);
			this.nextPicture.Name = "nextPicture";
			this.nextPicture.Size = new System.Drawing.Size(100, 50);
			this.nextPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.nextPicture.TabIndex = 2;
			this.nextPicture.TabStop = false;
			this.nextPicture.Visible = false;
			// 
			// currentLabel
			// 
			this.currentLabel.AutoSize = true;
			this.currentLabel.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.currentLabel.ForeColor = System.Drawing.Color.Yellow;
			this.currentLabel.Location = new System.Drawing.Point(308, 260);
			this.currentLabel.Name = "currentLabel";
			this.currentLabel.Size = new System.Drawing.Size(67, 23);
			this.currentLabel.TabIndex = 3;
			this.currentLabel.Text = "Current";
			this.currentLabel.Visible = false;
			// 
			// nextLabel
			// 
			this.nextLabel.AutoSize = true;
			this.nextLabel.BackColor = System.Drawing.Color.Black;
			this.nextLabel.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nextLabel.ForeColor = System.Drawing.Color.Yellow;
			this.nextLabel.Location = new System.Drawing.Point(207, 301);
			this.nextLabel.Name = "nextLabel";
			this.nextLabel.Size = new System.Drawing.Size(56, 23);
			this.nextLabel.TabIndex = 4;
			this.nextLabel.Text = "Next...";
			this.nextLabel.Visible = false;
			// 
			// startedTime
			// 
			this.startedTime.Interval = 2000;
			this.startedTime.Tick += new System.EventHandler(this.startedTime_Tick);
			// 
			// currentPicture
			// 
			this.currentPicture.Location = new System.Drawing.Point(157, 156);
			this.currentPicture.Name = "currentPicture";
			this.currentPicture.Size = new System.Drawing.Size(100, 50);
			this.currentPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.currentPicture.TabIndex = 5;
			this.currentPicture.TabStop = false;
			// 
			// netDisconnectedIcon
			// 
			this.netDisconnectedIcon.Image = ((System.Drawing.Image)(resources.GetObject("netDisconnectedIcon.Image")));
			this.netDisconnectedIcon.Location = new System.Drawing.Point(0, 0);
			this.netDisconnectedIcon.Name = "netDisconnectedIcon";
			this.netDisconnectedIcon.Size = new System.Drawing.Size(48, 48);
			this.netDisconnectedIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.netDisconnectedIcon.TabIndex = 6;
			this.netDisconnectedIcon.TabStop = false;
			this.netDisconnectedIcon.Visible = false;
			// 
			// ViewForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Black;
			this.ClientSize = new System.Drawing.Size(790, 379);
			this.ControlBox = false;
			this.Controls.Add(this.netDisconnectedIcon);
			this.Controls.Add(this.currentPicture);
			this.Controls.Add(this.currentLabel);
			this.Controls.Add(this.nextLabel);
			this.Controls.Add(this.nextPicture);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "ViewForm";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form_Load);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.VideoWindow_Paint);
			this.SizeChanged += new System.EventHandler(this.VideoWindow_SizeChanged);
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VideoWindow_FormClosing);
			((System.ComponentModel.ISupportInitialize)(this.nextPicture)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.currentPicture)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.netDisconnectedIcon)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox nextPicture;
		private System.Windows.Forms.Label currentLabel;
		private System.Windows.Forms.Label nextLabel;
		private System.Windows.Forms.Timer startedTime;
		private System.Windows.Forms.PictureBox currentPicture;
		private System.Windows.Forms.PictureBox netDisconnectedIcon;
	}
}

