﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace HovisPresent
{
	public enum NetworkMode { LocalOrMaster, Slave };
	public enum Monitor { noMonitor=-1, Monitor0, Monitor1, Monitor2, Monitor3, Monitor4, Monitor5, Monitor6 };

	/// <summary>
	/// Contains the options needed to run the presentation
	/// </summary>
	public class RunPresentationOptions : ApplicationSettingsBase
	{
		[UserScopedSetting()]
		[DefaultSettingValue("0")]
		public NetworkMode NetworkMode { 
			get { return (NetworkMode) this["NetworkMode"];}
			set { this["NetworkMode"] = value;}
		}
		
		/// <summary>
		/// Gets or sets the presentation screen.
		/// </summary>
		/// <value>The presentation screen. -1 for not shown</value>
		/// 
		[UserScopedSetting()]
		[DefaultSettingValue("0")]
		public Monitor PresentationMonitor {
			get { return (Monitor) this["PresentationMonitor"]; }
			set { this["PresentationMonitor"] = value; }
		}
		
		/// <summary>
		/// Gets or sets the presenter screen.
		/// </summary>
		/// <value>The presenter screen. -1 for not shown</value>
		[UserScopedSetting()]
		[DefaultSettingValue("0")]
		public Monitor PresenterMonitor {
			get { return (Monitor) this["PresenterMonitor"]; }
			set { this["PresenterMonitor"] = value; }
		}

		[UserScopedSetting()]
		[DefaultSettingValue("5322")]
		public int NetworkPort {
			get { return (int) this["NetworkPort"]; } 
			set { this["NetworkPort"] = value; }
		}

		[UserScopedSetting()]
		public List<string> Slaves {
			get { return (List<string>) this["Slaves"]; } 
			set { this["Slaves"] = value; }
		}

		[UserScopedSetting]
		[DefaultSettingValue("true")]
		public bool ShowScriptByDefault {
			get { return (bool)this["ShowScriptByDefault"]; }
			set { this["ShowScriptByDefault"] = value; }
		}

		/// <summary>
		/// Are we using fullscreen or windowed mode here?
		/// </summary>
		/// <returns></returns>
		public bool IsUsingFullscreen {
			get {
				return PresenterMonitor != PresentationMonitor;
			}

		}

		public RunPresentationOptions() {
			if (Slaves==null)
				Slaves = new List<string>();
		}

	}
}
