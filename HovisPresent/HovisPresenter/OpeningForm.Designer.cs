﻿namespace HovisPresent
{
	partial class OpeningForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OpeningForm));
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label2 = new System.Windows.Forms.Label();
			this.linkLabel8 = new System.Windows.Forms.LinkLabel();
			this.linkLabel7 = new System.Windows.Forms.LinkLabel();
			this.linkLabel6 = new System.Windows.Forms.LinkLabel();
			this.linkLabel5 = new System.Windows.Forms.LinkLabel();
			this.linkLabel4 = new System.Windows.Forms.LinkLabel();
			this.linkLabel3 = new System.Windows.Forms.LinkLabel();
			this.linkLabel2 = new System.Windows.Forms.LinkLabel();
			this.linkLabel1 = new System.Windows.Forms.LinkLabel();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.exit = new System.Windows.Forms.Button();
			this.loadPresentation = new System.Windows.Forms.Button();
			this.newPresentation = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(65, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(247, 35);
			this.label1.TabIndex = 0;
			this.label1.Text = "Hovis Presenter";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.linkLabel8);
			this.groupBox1.Controls.Add(this.linkLabel7);
			this.groupBox1.Controls.Add(this.linkLabel6);
			this.groupBox1.Controls.Add(this.linkLabel5);
			this.groupBox1.Controls.Add(this.linkLabel4);
			this.groupBox1.Controls.Add(this.linkLabel3);
			this.groupBox1.Controls.Add(this.linkLabel2);
			this.groupBox1.Controls.Add(this.linkLabel1);
			this.groupBox1.Location = new System.Drawing.Point(12, 47);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(204, 177);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(46, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(108, 19);
			this.label2.TabIndex = 8;
			this.label2.Text = "Recent Files";
			// 
			// linkLabel8
			// 
			this.linkLabel8.Location = new System.Drawing.Point(7, 157);
			this.linkLabel8.Name = "linkLabel8";
			this.linkLabel8.Size = new System.Drawing.Size(191, 16);
			this.linkLabel8.TabIndex = 7;
			this.linkLabel8.TabStop = true;
			this.linkLabel8.Text = "linkLabel8";
			this.linkLabel8.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel8_LinkClicked);
			// 
			// linkLabel7
			// 
			this.linkLabel7.Location = new System.Drawing.Point(7, 141);
			this.linkLabel7.Name = "linkLabel7";
			this.linkLabel7.Size = new System.Drawing.Size(191, 16);
			this.linkLabel7.TabIndex = 6;
			this.linkLabel7.TabStop = true;
			this.linkLabel7.Text = "linkLabel7";
			this.linkLabel7.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel8_LinkClicked);
			// 
			// linkLabel6
			// 
			this.linkLabel6.Location = new System.Drawing.Point(7, 125);
			this.linkLabel6.Name = "linkLabel6";
			this.linkLabel6.Size = new System.Drawing.Size(191, 16);
			this.linkLabel6.TabIndex = 5;
			this.linkLabel6.TabStop = true;
			this.linkLabel6.Text = "linkLabel6";
			this.linkLabel6.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel8_LinkClicked);
			// 
			// linkLabel5
			// 
			this.linkLabel5.Location = new System.Drawing.Point(7, 109);
			this.linkLabel5.Name = "linkLabel5";
			this.linkLabel5.Size = new System.Drawing.Size(191, 16);
			this.linkLabel5.TabIndex = 4;
			this.linkLabel5.TabStop = true;
			this.linkLabel5.Text = "linkLabel5";
			this.linkLabel5.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel8_LinkClicked);
			// 
			// linkLabel4
			// 
			this.linkLabel4.Location = new System.Drawing.Point(7, 93);
			this.linkLabel4.Name = "linkLabel4";
			this.linkLabel4.Size = new System.Drawing.Size(191, 16);
			this.linkLabel4.TabIndex = 3;
			this.linkLabel4.TabStop = true;
			this.linkLabel4.Text = "linkLabel4";
			this.linkLabel4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel8_LinkClicked);
			// 
			// linkLabel3
			// 
			this.linkLabel3.Location = new System.Drawing.Point(7, 77);
			this.linkLabel3.Name = "linkLabel3";
			this.linkLabel3.Size = new System.Drawing.Size(191, 16);
			this.linkLabel3.TabIndex = 2;
			this.linkLabel3.TabStop = true;
			this.linkLabel3.Text = "linkLabel3";
			this.linkLabel3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel8_LinkClicked);
			// 
			// linkLabel2
			// 
			this.linkLabel2.Location = new System.Drawing.Point(7, 61);
			this.linkLabel2.Name = "linkLabel2";
			this.linkLabel2.Size = new System.Drawing.Size(191, 16);
			this.linkLabel2.TabIndex = 1;
			this.linkLabel2.TabStop = true;
			this.linkLabel2.Text = "linkLabel2";
			this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel8_LinkClicked);
			// 
			// linkLabel1
			// 
			this.linkLabel1.Location = new System.Drawing.Point(7, 45);
			this.linkLabel1.Name = "linkLabel1";
			this.linkLabel1.Size = new System.Drawing.Size(191, 16);
			this.linkLabel1.TabIndex = 0;
			this.linkLabel1.TabStop = true;
			this.linkLabel1.Text = "linkLabel1";
			this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel8_LinkClicked);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.exit);
			this.groupBox2.Controls.Add(this.loadPresentation);
			this.groupBox2.Controls.Add(this.newPresentation);
			this.groupBox2.Controls.Add(this.label3);
			this.groupBox2.Location = new System.Drawing.Point(222, 47);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(143, 177);
			this.groupBox2.TabIndex = 2;
			this.groupBox2.TabStop = false;
			// 
			// exit
			// 
			this.exit.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.exit.Location = new System.Drawing.Point(7, 139);
			this.exit.Name = "exit";
			this.exit.Size = new System.Drawing.Size(130, 31);
			this.exit.TabIndex = 12;
			this.exit.Text = "Exit";
			this.exit.UseVisualStyleBackColor = true;
			this.exit.Click += new System.EventHandler(this.exit_Click);
			// 
			// loadPresentation
			// 
			this.loadPresentation.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.loadPresentation.Location = new System.Drawing.Point(7, 92);
			this.loadPresentation.Name = "loadPresentation";
			this.loadPresentation.Size = new System.Drawing.Size(130, 43);
			this.loadPresentation.TabIndex = 11;
			this.loadPresentation.Text = "Load Presentation";
			this.loadPresentation.UseVisualStyleBackColor = true;
			this.loadPresentation.Click += new System.EventHandler(this.loadPresentation_Click);
			// 
			// newPresentation
			// 
			this.newPresentation.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.newPresentation.Location = new System.Drawing.Point(7, 45);
			this.newPresentation.Name = "newPresentation";
			this.newPresentation.Size = new System.Drawing.Size(130, 43);
			this.newPresentation.TabIndex = 10;
			this.newPresentation.Text = "New Presentation";
			this.newPresentation.UseVisualStyleBackColor = true;
			this.newPresentation.Click += new System.EventHandler(this.newPresentation_Click);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(38, 16);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(69, 19);
			this.label3.TabIndex = 9;
			this.label3.Text = "Actions";
			// 
			// OpeningForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(377, 236);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "OpeningForm";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "Hovis Presenter";
			this.Load += new System.EventHandler(this.OpeningForm_Load);
			this.VisibleChanged += new System.EventHandler(this.OpeningForm_VisibleChanged);
			this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OpeningForm_MouseDown);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.LinkLabel linkLabel8;
		private System.Windows.Forms.LinkLabel linkLabel7;
		private System.Windows.Forms.LinkLabel linkLabel6;
		private System.Windows.Forms.LinkLabel linkLabel5;
		private System.Windows.Forms.LinkLabel linkLabel4;
		private System.Windows.Forms.LinkLabel linkLabel3;
		private System.Windows.Forms.LinkLabel linkLabel2;
		private System.Windows.Forms.LinkLabel linkLabel1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button newPresentation;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button loadPresentation;
		private System.Windows.Forms.Button exit;
	}
}