﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace HovisPresent
{
	public partial class Options : Form
	{
		public Font ScriptFont { get; set; }

		public Options() {
			InitializeComponent();
		}

		private void Options_Load(object sender, EventArgs e) {
			thumbSize.SelectedIndex = Properties.Settings.Default.ThumbnailSize;
			fontDescription.Text = ScriptFont.FontFamily.Name + " " +
									ScriptFont.SizeInPoints + "pt" +
									(ScriptFont.Bold ? "Bold " : "") +
									(ScriptFont.Italic ? "Italic " : "");
			
			int layout = Properties.Settings.Default.PresenterWindowLayout;
			scriptBottom.Checked =  layout != 1;
			scriptLeft.Checked = layout == 1;
		}

		private void changeFont_Click(object sender, EventArgs e) {
			FontDialog fd = new FontDialog();
			fd.Font = ScriptFont;
			fd.FontMustExist = true;
			fd.ShowColor = false;
			if (fd.ShowDialog(this) == DialogResult.OK) {
				ScriptFont = fd.Font;
			}
		}

		public PresenterWindowLayout PresenterWindowLayout {
			get {
				return scriptLeft.Checked ? PresenterWindowLayout.ScriptLeft : PresenterWindowLayout.ScriptBottom;
			}
		}
	}
}
