﻿namespace HovisPresent
{
	partial class RunForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RunForm));
			this.monitorGroup = new System.Windows.Forms.GroupBox();
			this.enablePresenterMonitor = new System.Windows.Forms.CheckBox();
			this.enablePresentationMonitor = new System.Windows.Forms.CheckBox();
			this.presenterMonitor = new System.Windows.Forms.ComboBox();
			this.presentationMonitor = new System.Windows.Forms.ComboBox();
			this.networkControlSettings = new System.Windows.Forms.GroupBox();
			this.slaves = new System.Windows.Forms.TextBox();
			this.slaveLabel = new System.Windows.Forms.Label();
			this.networkPort = new System.Windows.Forms.NumericUpDown();
			this.label4 = new System.Windows.Forms.Label();
			this.networkSlave = new System.Windows.Forms.RadioButton();
			this.localHostname = new System.Windows.Forms.TextBox();
			this.networkMasterOrLocal = new System.Windows.Forms.RadioButton();
			this.ipBoxLabel = new System.Windows.Forms.Label();
			this.run = new System.Windows.Forms.Button();
			this.cancel = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.defaultShowScript = new System.Windows.Forms.CheckBox();
			this.monitorGroup.SuspendLayout();
			this.networkControlSettings.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.networkPort)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// monitorGroup
			// 
			this.monitorGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.monitorGroup.Controls.Add(this.enablePresenterMonitor);
			this.monitorGroup.Controls.Add(this.enablePresentationMonitor);
			this.monitorGroup.Controls.Add(this.presenterMonitor);
			this.monitorGroup.Controls.Add(this.presentationMonitor);
			this.monitorGroup.Location = new System.Drawing.Point(12, 12);
			this.monitorGroup.Name = "monitorGroup";
			this.monitorGroup.Size = new System.Drawing.Size(378, 79);
			this.monitorGroup.TabIndex = 0;
			this.monitorGroup.TabStop = false;
			this.monitorGroup.Text = "Monitor Setup";
			// 
			// enablePresenterView
			// 
			this.enablePresenterMonitor.AutoSize = true;
			this.enablePresenterMonitor.Location = new System.Drawing.Point(10, 48);
			this.enablePresenterMonitor.Name = "enablePresenterView";
			this.enablePresenterMonitor.Size = new System.Drawing.Size(97, 17);
			this.enablePresenterMonitor.TabIndex = 2;
			this.enablePresenterMonitor.Text = "Presenter View";
			this.enablePresenterMonitor.UseVisualStyleBackColor = true;
			this.enablePresenterMonitor.CheckedChanged += new System.EventHandler(this.enablePresenterView_CheckedChanged);
			// 
			// enablePresentationView
			// 
			this.enablePresentationMonitor.AutoSize = true;
			this.enablePresentationMonitor.Location = new System.Drawing.Point(10, 23);
			this.enablePresentationMonitor.Name = "enablePresentationView";
			this.enablePresentationMonitor.Size = new System.Drawing.Size(111, 17);
			this.enablePresentationMonitor.TabIndex = 0;
			this.enablePresentationMonitor.Text = "Presentation View";
			this.enablePresentationMonitor.UseVisualStyleBackColor = true;
			this.enablePresentationMonitor.CheckedChanged += new System.EventHandler(this.enablePresentationView_CheckedChanged);
			// 
			// presenterScreen
			// 
			this.presenterMonitor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.presenterMonitor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.presenterMonitor.FormattingEnabled = true;
			this.presenterMonitor.Location = new System.Drawing.Point(127, 46);
			this.presenterMonitor.Name = "presenterScreen";
			this.presenterMonitor.Size = new System.Drawing.Size(241, 21);
			this.presenterMonitor.TabIndex = 3;
			// 
			// presentationScreen
			// 
			this.presentationMonitor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.presentationMonitor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.presentationMonitor.FormattingEnabled = true;
			this.presentationMonitor.Location = new System.Drawing.Point(127, 21);
			this.presentationMonitor.Name = "presentationScreen";
			this.presentationMonitor.Size = new System.Drawing.Size(241, 21);
			this.presentationMonitor.TabIndex = 1;
			// 
			// networkControlSettings
			// 
			this.networkControlSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.networkControlSettings.Controls.Add(this.slaves);
			this.networkControlSettings.Controls.Add(this.slaveLabel);
			this.networkControlSettings.Controls.Add(this.networkPort);
			this.networkControlSettings.Controls.Add(this.label4);
			this.networkControlSettings.Controls.Add(this.networkSlave);
			this.networkControlSettings.Controls.Add(this.localHostname);
			this.networkControlSettings.Controls.Add(this.networkMasterOrLocal);
			this.networkControlSettings.Controls.Add(this.ipBoxLabel);
			this.networkControlSettings.Location = new System.Drawing.Point(13, 97);
			this.networkControlSettings.Name = "networkControlSettings";
			this.networkControlSettings.Size = new System.Drawing.Size(377, 169);
			this.networkControlSettings.TabIndex = 1;
			this.networkControlSettings.TabStop = false;
			this.networkControlSettings.Text = "Network/Control Settings";
			// 
			// slaves
			// 
			this.slaves.AcceptsReturn = true;
			this.slaves.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.slaves.Location = new System.Drawing.Point(117, 92);
			this.slaves.Multiline = true;
			this.slaves.Name = "slaves";
			this.slaves.Size = new System.Drawing.Size(250, 67);
			this.slaves.TabIndex = 7;
			this.slaves.WordWrap = false;
			// 
			// slaveLabel
			// 
			this.slaveLabel.AutoSize = true;
			this.slaveLabel.Location = new System.Drawing.Point(72, 92);
			this.slaveLabel.Name = "slaveLabel";
			this.slaveLabel.Size = new System.Drawing.Size(39, 13);
			this.slaveLabel.TabIndex = 6;
			this.slaveLabel.Text = "Sla&ves";
			this.slaveLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// networkPort
			// 
			this.networkPort.Location = new System.Drawing.Point(117, 66);
			this.networkPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
			this.networkPort.Minimum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
			this.networkPort.Name = "networkPort";
			this.networkPort.Size = new System.Drawing.Size(63, 20);
			this.networkPort.TabIndex = 5;
			this.networkPort.Value = new decimal(new int[] {
            1024,
            0,
            0,
            0});
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(85, 68);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(26, 13);
			this.label4.TabIndex = 4;
			this.label4.Text = "Por&t";
			this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// networkSlave
			// 
			this.networkSlave.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.networkSlave.AutoSize = true;
			this.networkSlave.Location = new System.Drawing.Point(188, 19);
			this.networkSlave.Name = "networkSlave";
			this.networkSlave.Size = new System.Drawing.Size(140, 17);
			this.networkSlave.TabIndex = 1;
			this.networkSlave.Text = "&Slave (No Local Control)";
			this.networkSlave.UseVisualStyleBackColor = true;
			this.networkSlave.Click += new System.EventHandler(this.networkTypeChanged);
			// 
			// localHostname
			// 
			this.localHostname.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.localHostname.Location = new System.Drawing.Point(117, 40);
			this.localHostname.Name = "localHostname";
			this.localHostname.ReadOnly = true;
			this.localHostname.Size = new System.Drawing.Size(250, 20);
			this.localHostname.TabIndex = 3;
			// 
			// networkMasterOrLocal
			// 
			this.networkMasterOrLocal.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.networkMasterOrLocal.AutoSize = true;
			this.networkMasterOrLocal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.networkMasterOrLocal.Location = new System.Drawing.Point(49, 19);
			this.networkMasterOrLocal.Name = "networkMasterOrLocal";
			this.networkMasterOrLocal.Size = new System.Drawing.Size(129, 17);
			this.networkMasterOrLocal.TabIndex = 0;
			this.networkMasterOrLocal.TabStop = true;
			this.networkMasterOrLocal.Text = "&Master/Local Only";
			this.networkMasterOrLocal.UseVisualStyleBackColor = true;
			this.networkMasterOrLocal.Click += new System.EventHandler(this.networkTypeChanged);
			// 
			// ipBoxLabel
			// 
			this.ipBoxLabel.AutoSize = true;
			this.ipBoxLabel.Location = new System.Drawing.Point(6, 43);
			this.ipBoxLabel.Name = "ipBoxLabel";
			this.ipBoxLabel.Size = new System.Drawing.Size(105, 13);
			this.ipBoxLabel.TabIndex = 2;
			this.ipBoxLabel.Text = "Local Hostname / IP";
			this.ipBoxLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// run
			// 
			this.run.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.run.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.run.Location = new System.Drawing.Point(101, 327);
			this.run.Name = "run";
			this.run.Size = new System.Drawing.Size(120, 23);
			this.run.TabIndex = 3;
			this.run.Text = "&Run Presentation";
			this.run.UseVisualStyleBackColor = true;
			this.run.Click += new System.EventHandler(this.run_Click);
			// 
			// cancel
			// 
			this.cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancel.Location = new System.Drawing.Point(227, 327);
			this.cancel.Name = "cancel";
			this.cancel.Size = new System.Drawing.Size(75, 23);
			this.cancel.TabIndex = 4;
			this.cancel.Text = "&Cancel";
			this.cancel.UseVisualStyleBackColor = true;
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.defaultShowScript);
			this.groupBox1.Location = new System.Drawing.Point(12, 272);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(378, 49);
			this.groupBox1.TabIndex = 2;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Other Options";
			// 
			// defaultShowScript
			// 
			this.defaultShowScript.AutoSize = true;
			this.defaultShowScript.Location = new System.Drawing.Point(78, 19);
			this.defaultShowScript.Name = "defaultShowScript";
			this.defaultShowScript.Size = new System.Drawing.Size(223, 17);
			this.defaultShowScript.TabIndex = 0;
			this.defaultShowScript.Text = "Show S&cript by Default on Presenter View";
			this.defaultShowScript.UseVisualStyleBackColor = true;
			// 
			// RunForm
			// 
			this.AcceptButton = this.run;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.cancel;
			this.ClientSize = new System.Drawing.Size(402, 362);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.cancel);
			this.Controls.Add(this.run);
			this.Controls.Add(this.networkControlSettings);
			this.Controls.Add(this.monitorGroup);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MinimumSize = new System.Drawing.Size(410, 370);
			this.Name = "RunForm";
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
			this.Text = "Run Presentation";
			this.Load += new System.EventHandler(this.RunForm_Load);
			this.monitorGroup.ResumeLayout(false);
			this.monitorGroup.PerformLayout();
			this.networkControlSettings.ResumeLayout(false);
			this.networkControlSettings.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.networkPort)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox monitorGroup;
		private System.Windows.Forms.GroupBox networkControlSettings;
		private System.Windows.Forms.RadioButton networkSlave;
		private System.Windows.Forms.RadioButton networkMasterOrLocal;
		private System.Windows.Forms.TextBox localHostname;
		private System.Windows.Forms.Label ipBoxLabel;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.NumericUpDown networkPort;
		private System.Windows.Forms.Button run;
		private System.Windows.Forms.Button cancel;
		private System.Windows.Forms.Label slaveLabel;
		private System.Windows.Forms.TextBox slaves;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.CheckBox defaultShowScript;
		private System.Windows.Forms.ComboBox presentationMonitor;
		private System.Windows.Forms.ComboBox presenterMonitor;
		private System.Windows.Forms.CheckBox enablePresenterMonitor;
		private System.Windows.Forms.CheckBox enablePresentationMonitor;

	}
}