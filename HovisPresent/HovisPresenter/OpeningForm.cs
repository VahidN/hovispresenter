﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.InteropServices;

namespace HovisPresent
{
	public partial class OpeningForm : Form
	{
		private MainWindow mainWindow;

		public OpeningForm() {
			InitializeComponent();
		}

		private void linkLabel8_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
			LinkLabel label = (LinkLabel)sender;
			if (!mainWindow.OpenFile((string)label.Tag))
				return;
			ShowMainWindow();
		}

		private void ShowMainWindow() {
			Hide();
			mainWindow.Show();
			mainWindow.BringToFront();
		}

		private void newPresentation_Click(object sender, EventArgs e) {
			if (mainWindow.NewPresentation())
				ShowMainWindow();
		}

		private void loadPresentation_Click(object sender, EventArgs e) {
			if (mainWindow.OpenFile())
				ShowMainWindow();
		}

		public void exit_Click(object sender, EventArgs e) {
//			mainWindow.Close();
			Close();
		}

		private void OpeningForm_Load(object sender, EventArgs e) {
			mainWindow = new MainWindow();
			mainWindow.OpeningForm = this;
			mainWindow.Owner = this;
		}

		// move form on background click

		private void OpeningForm_MouseDown(object sender, MouseEventArgs e) {
			if (e.Button == MouseButtons.Left) {
				Win32Stuff.ReleaseCapture();
				Win32Stuff.SendMessage(Handle, Win32Stuff.WM_NCLBUTTONDOWN, Win32Stuff.HTCAPTION, 0);
			} 
		}

		private void OpeningForm_VisibleChanged(object sender, EventArgs e) {
			if (!Visible)
				return;

			var MRU = Properties.Settings.Default.RecentFiles;

			LinkLabel[] links = { linkLabel1, linkLabel2, linkLabel3, linkLabel4, linkLabel5, linkLabel6, linkLabel7, linkLabel8 };

			foreach (LinkLabel l in links)
				l.Text = "";

			if (MRU == null)
				return;

			int label = 0;
			for (int i = MRU.Count - 1; i >= 0; i--) {
				LinkLabel l = links[label++];
				l.Text = Path.GetFileNameWithoutExtension(MRU[i]);
				l.Tag = MRU[i];
			}
		}

	}
}
