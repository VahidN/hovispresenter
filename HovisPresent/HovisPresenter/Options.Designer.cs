﻿namespace HovisPresent
{
	partial class Options
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Options));
			this.thumbSize = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.fontDescription = new System.Windows.Forms.Label();
			this.changeFont = new System.Windows.Forms.Button();
			this.ok = new System.Windows.Forms.Button();
			this.cancel = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.label13 = new System.Windows.Forms.Label();
			this.endSlideshowAlt = new HovisPresent.ShortcutKeyControl.ShortcutKeyControl();
			this.label14 = new System.Windows.Forms.Label();
			this.endSlideshow = new HovisPresent.ShortcutKeyControl.ShortcutKeyControl();
			this.label11 = new System.Windows.Forms.Label();
			this.lastSlideAlt = new HovisPresent.ShortcutKeyControl.ShortcutKeyControl();
			this.label12 = new System.Windows.Forms.Label();
			this.lastSlide = new HovisPresent.ShortcutKeyControl.ShortcutKeyControl();
			this.label9 = new System.Windows.Forms.Label();
			this.firstSlideAlt = new HovisPresent.ShortcutKeyControl.ShortcutKeyControl();
			this.label10 = new System.Windows.Forms.Label();
			this.firstSlide = new HovisPresent.ShortcutKeyControl.ShortcutKeyControl();
			this.label7 = new System.Windows.Forms.Label();
			this.toggleScriptAlt = new HovisPresent.ShortcutKeyControl.ShortcutKeyControl();
			this.label8 = new System.Windows.Forms.Label();
			this.toggleScript = new HovisPresent.ShortcutKeyControl.ShortcutKeyControl();
			this.label5 = new System.Windows.Forms.Label();
			this.prevSlideAlt = new HovisPresent.ShortcutKeyControl.ShortcutKeyControl();
			this.label6 = new System.Windows.Forms.Label();
			this.prevSlide = new HovisPresent.ShortcutKeyControl.ShortcutKeyControl();
			this.label4 = new System.Windows.Forms.Label();
			this.nextSlideAlt = new HovisPresent.ShortcutKeyControl.ShortcutKeyControl();
			this.label3 = new System.Windows.Forms.Label();
			this.nextSlide = new HovisPresent.ShortcutKeyControl.ShortcutKeyControl();
			this.label15 = new System.Windows.Forms.Label();
			this.scriptBottom = new System.Windows.Forms.RadioButton();
			this.scriptLeft = new System.Windows.Forms.RadioButton();
			this.label16 = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// thumbSize
			// 
			this.thumbSize.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.thumbSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.thumbSize.FormattingEnabled = true;
			this.thumbSize.Items.AddRange(new object[] {
            "Small",
            "Medium",
            "Large",
            "Widescreen Small",
            "Widescreen Medium",
            "Widescreen Large"});
			this.thumbSize.Location = new System.Drawing.Point(183, 22);
			this.thumbSize.Name = "thumbSize";
			this.thumbSize.Size = new System.Drawing.Size(140, 21);
			this.thumbSize.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(98, 25);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(82, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "&Thumbnail Size:";
			// 
			// label2
			// 
			this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(98, 48);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(61, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Script &Font:";
			// 
			// fontDescription
			// 
			this.fontDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.fontDescription.Location = new System.Drawing.Point(180, 48);
			this.fontDescription.Name = "fontDescription";
			this.fontDescription.Size = new System.Drawing.Size(234, 13);
			this.fontDescription.TabIndex = 3;
			this.fontDescription.Text = "label3";
			// 
			// changeFont
			// 
			this.changeFont.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.changeFont.Location = new System.Drawing.Point(183, 64);
			this.changeFont.Name = "changeFont";
			this.changeFont.Size = new System.Drawing.Size(105, 23);
			this.changeFont.TabIndex = 4;
			this.changeFont.Text = "&Change Font";
			this.changeFont.UseVisualStyleBackColor = true;
			this.changeFont.Click += new System.EventHandler(this.changeFont_Click);
			// 
			// ok
			// 
			this.ok.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.ok.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.ok.Location = new System.Drawing.Point(139, 334);
			this.ok.Name = "ok";
			this.ok.Size = new System.Drawing.Size(75, 23);
			this.ok.TabIndex = 5;
			this.ok.Text = "OK";
			this.ok.UseVisualStyleBackColor = true;
			// 
			// cancel
			// 
			this.cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancel.Location = new System.Drawing.Point(231, 334);
			this.cancel.Name = "cancel";
			this.cancel.Size = new System.Drawing.Size(75, 23);
			this.cancel.TabIndex = 6;
			this.cancel.Text = "Cancel";
			this.cancel.UseVisualStyleBackColor = true;
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.label16);
			this.groupBox1.Controls.Add(this.scriptLeft);
			this.groupBox1.Controls.Add(this.scriptBottom);
			this.groupBox1.Controls.Add(this.label15);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.thumbSize);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.changeFont);
			this.groupBox1.Controls.Add(this.fontDescription);
			this.groupBox1.Location = new System.Drawing.Point(12, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(420, 121);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "View";
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this.label13);
			this.groupBox2.Controls.Add(this.endSlideshowAlt);
			this.groupBox2.Controls.Add(this.label14);
			this.groupBox2.Controls.Add(this.endSlideshow);
			this.groupBox2.Controls.Add(this.label11);
			this.groupBox2.Controls.Add(this.lastSlideAlt);
			this.groupBox2.Controls.Add(this.label12);
			this.groupBox2.Controls.Add(this.lastSlide);
			this.groupBox2.Controls.Add(this.label9);
			this.groupBox2.Controls.Add(this.firstSlideAlt);
			this.groupBox2.Controls.Add(this.label10);
			this.groupBox2.Controls.Add(this.firstSlide);
			this.groupBox2.Controls.Add(this.label7);
			this.groupBox2.Controls.Add(this.toggleScriptAlt);
			this.groupBox2.Controls.Add(this.label8);
			this.groupBox2.Controls.Add(this.toggleScript);
			this.groupBox2.Controls.Add(this.label5);
			this.groupBox2.Controls.Add(this.prevSlideAlt);
			this.groupBox2.Controls.Add(this.label6);
			this.groupBox2.Controls.Add(this.prevSlide);
			this.groupBox2.Controls.Add(this.label4);
			this.groupBox2.Controls.Add(this.nextSlideAlt);
			this.groupBox2.Controls.Add(this.label3);
			this.groupBox2.Controls.Add(this.nextSlide);
			this.groupBox2.Location = new System.Drawing.Point(13, 139);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(419, 186);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Keys";
			// 
			// label13
			// 
			this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(236, 152);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(16, 13);
			this.label13.TabIndex = 22;
			this.label13.Text = "or";
			// 
			// endSlideshowAlt
			// 
			this.endSlideshowAlt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.endSlideshowAlt.DataBindings.Add(new System.Windows.Forms.Binding("Key", global::HovisPresent.Properties.Settings.Default, "KeyAltEndSlideshow", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.endSlideshowAlt.Key = global::HovisPresent.Properties.Settings.Default.KeyAltEndSlideshow;
			this.endSlideshowAlt.Location = new System.Drawing.Point(260, 149);
			this.endSlideshowAlt.Multiline = true;
			this.endSlideshowAlt.Name = "endSlideshowAlt";
			this.endSlideshowAlt.Size = new System.Drawing.Size(144, 20);
			this.endSlideshowAlt.TabIndex = 23;
			this.endSlideshowAlt.Text = "None";
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(10, 152);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(80, 13);
			this.label14.TabIndex = 20;
			this.label14.Text = "&End Slideshow:";
			// 
			// endSlideshow
			// 
			this.endSlideshow.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.endSlideshow.DataBindings.Add(new System.Windows.Forms.Binding("Key", global::HovisPresent.Properties.Settings.Default, "KeyEndSlideshow", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.endSlideshow.Key = global::HovisPresent.Properties.Settings.Default.KeyEndSlideshow;
			this.endSlideshow.Location = new System.Drawing.Point(90, 149);
			this.endSlideshow.Multiline = true;
			this.endSlideshow.Name = "endSlideshow";
			this.endSlideshow.Size = new System.Drawing.Size(138, 20);
			this.endSlideshow.TabIndex = 21;
			this.endSlideshow.Text = "None";
			// 
			// label11
			// 
			this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(236, 126);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(16, 13);
			this.label11.TabIndex = 18;
			this.label11.Text = "or";
			// 
			// lastSlideAlt
			// 
			this.lastSlideAlt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.lastSlideAlt.DataBindings.Add(new System.Windows.Forms.Binding("Key", global::HovisPresent.Properties.Settings.Default, "KeyAltLastSlide", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.lastSlideAlt.Key = global::HovisPresent.Properties.Settings.Default.KeyAltLastSlide;
			this.lastSlideAlt.Location = new System.Drawing.Point(260, 123);
			this.lastSlideAlt.Multiline = true;
			this.lastSlideAlt.Name = "lastSlideAlt";
			this.lastSlideAlt.Size = new System.Drawing.Size(144, 20);
			this.lastSlideAlt.TabIndex = 19;
			this.lastSlideAlt.Text = "None";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(10, 126);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(56, 13);
			this.label12.TabIndex = 16;
			this.label12.Text = "&Last Slide:";
			// 
			// lastSlide
			// 
			this.lastSlide.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.lastSlide.DataBindings.Add(new System.Windows.Forms.Binding("Key", global::HovisPresent.Properties.Settings.Default, "KeyLastSlide", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.lastSlide.Key = global::HovisPresent.Properties.Settings.Default.KeyLastSlide;
			this.lastSlide.Location = new System.Drawing.Point(90, 123);
			this.lastSlide.Multiline = true;
			this.lastSlide.Name = "lastSlide";
			this.lastSlide.Size = new System.Drawing.Size(138, 20);
			this.lastSlide.TabIndex = 17;
			this.lastSlide.Text = "None";
			// 
			// label9
			// 
			this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(236, 100);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(16, 13);
			this.label9.TabIndex = 14;
			this.label9.Text = "or";
			// 
			// firstSlideAlt
			// 
			this.firstSlideAlt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.firstSlideAlt.DataBindings.Add(new System.Windows.Forms.Binding("Key", global::HovisPresent.Properties.Settings.Default, "KeyAltFirstSlide", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.firstSlideAlt.Key = global::HovisPresent.Properties.Settings.Default.KeyAltFirstSlide;
			this.firstSlideAlt.Location = new System.Drawing.Point(260, 97);
			this.firstSlideAlt.Multiline = true;
			this.firstSlideAlt.Name = "firstSlideAlt";
			this.firstSlideAlt.Size = new System.Drawing.Size(144, 20);
			this.firstSlideAlt.TabIndex = 15;
			this.firstSlideAlt.Text = "None";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(10, 100);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(55, 13);
			this.label10.TabIndex = 12;
			this.label10.Text = "&First Slide:";
			// 
			// firstSlide
			// 
			this.firstSlide.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.firstSlide.DataBindings.Add(new System.Windows.Forms.Binding("Key", global::HovisPresent.Properties.Settings.Default, "KeyFirstSlide", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.firstSlide.Key = global::HovisPresent.Properties.Settings.Default.KeyFirstSlide;
			this.firstSlide.Location = new System.Drawing.Point(90, 97);
			this.firstSlide.Multiline = true;
			this.firstSlide.Name = "firstSlide";
			this.firstSlide.Size = new System.Drawing.Size(138, 20);
			this.firstSlide.TabIndex = 13;
			this.firstSlide.Text = "None";
			// 
			// label7
			// 
			this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(236, 74);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(16, 13);
			this.label7.TabIndex = 10;
			this.label7.Text = "or";
			// 
			// toggleScriptAlt
			// 
			this.toggleScriptAlt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.toggleScriptAlt.DataBindings.Add(new System.Windows.Forms.Binding("Key", global::HovisPresent.Properties.Settings.Default, "KeyAltToggleScript", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.toggleScriptAlt.Key = global::HovisPresent.Properties.Settings.Default.KeyAltToggleScript;
			this.toggleScriptAlt.Location = new System.Drawing.Point(260, 71);
			this.toggleScriptAlt.Multiline = true;
			this.toggleScriptAlt.Name = "toggleScriptAlt";
			this.toggleScriptAlt.Size = new System.Drawing.Size(144, 20);
			this.toggleScriptAlt.TabIndex = 11;
			this.toggleScriptAlt.Text = "None";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(10, 74);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(73, 13);
			this.label8.TabIndex = 8;
			this.label8.Text = "Toggle &Script:";
			// 
			// toggleScript
			// 
			this.toggleScript.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.toggleScript.DataBindings.Add(new System.Windows.Forms.Binding("Key", global::HovisPresent.Properties.Settings.Default, "KeyToggleScript", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.toggleScript.Key = global::HovisPresent.Properties.Settings.Default.KeyToggleScript;
			this.toggleScript.Location = new System.Drawing.Point(90, 71);
			this.toggleScript.Multiline = true;
			this.toggleScript.Name = "toggleScript";
			this.toggleScript.Size = new System.Drawing.Size(138, 20);
			this.toggleScript.TabIndex = 9;
			this.toggleScript.Text = "None";
			// 
			// label5
			// 
			this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(236, 48);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(16, 13);
			this.label5.TabIndex = 6;
			this.label5.Text = "or";
			// 
			// prevSlideAlt
			// 
			this.prevSlideAlt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.prevSlideAlt.DataBindings.Add(new System.Windows.Forms.Binding("Key", global::HovisPresent.Properties.Settings.Default, "KeyAltPreviousSlide", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.prevSlideAlt.Key = global::HovisPresent.Properties.Settings.Default.KeyAltPreviousSlide;
			this.prevSlideAlt.Location = new System.Drawing.Point(260, 45);
			this.prevSlideAlt.Multiline = true;
			this.prevSlideAlt.Name = "prevSlideAlt";
			this.prevSlideAlt.Size = new System.Drawing.Size(144, 20);
			this.prevSlideAlt.TabIndex = 7;
			this.prevSlideAlt.Text = "None";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(10, 48);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(77, 13);
			this.label6.TabIndex = 4;
			this.label6.Text = "&Previous Slide:";
			// 
			// prevSlide
			// 
			this.prevSlide.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.prevSlide.DataBindings.Add(new System.Windows.Forms.Binding("Key", global::HovisPresent.Properties.Settings.Default, "KeyPreviousSlide", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.prevSlide.Key = global::HovisPresent.Properties.Settings.Default.KeyPreviousSlide;
			this.prevSlide.Location = new System.Drawing.Point(90, 45);
			this.prevSlide.Multiline = true;
			this.prevSlide.Name = "prevSlide";
			this.prevSlide.Size = new System.Drawing.Size(138, 20);
			this.prevSlide.TabIndex = 5;
			this.prevSlide.Text = "None";
			// 
			// label4
			// 
			this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(236, 22);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(16, 13);
			this.label4.TabIndex = 2;
			this.label4.Text = "or";
			// 
			// nextSlideAlt
			// 
			this.nextSlideAlt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.nextSlideAlt.DataBindings.Add(new System.Windows.Forms.Binding("Key", global::HovisPresent.Properties.Settings.Default, "KeyAltNextSlide", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.nextSlideAlt.Key = global::HovisPresent.Properties.Settings.Default.KeyAltNextSlide;
			this.nextSlideAlt.Location = new System.Drawing.Point(260, 19);
			this.nextSlideAlt.Multiline = true;
			this.nextSlideAlt.Name = "nextSlideAlt";
			this.nextSlideAlt.Size = new System.Drawing.Size(144, 20);
			this.nextSlideAlt.TabIndex = 3;
			this.nextSlideAlt.Text = "None";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(10, 22);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(58, 13);
			this.label3.TabIndex = 0;
			this.label3.Text = "&Next Slide:";
			// 
			// nextSlide
			// 
			this.nextSlide.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.nextSlide.DataBindings.Add(new System.Windows.Forms.Binding("Key", global::HovisPresent.Properties.Settings.Default, "KeyNextSlide", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.nextSlide.Key = global::HovisPresent.Properties.Settings.Default.KeyNextSlide;
			this.nextSlide.Location = new System.Drawing.Point(90, 19);
			this.nextSlide.Multiline = true;
			this.nextSlide.Name = "nextSlide";
			this.nextSlide.Size = new System.Drawing.Size(138, 20);
			this.nextSlide.TabIndex = 1;
			this.nextSlide.Text = "None";
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(70, 96);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(82, 13);
			this.label15.TabIndex = 5;
			this.label15.Text = "S&how Script on:";
			// 
			// scriptBottom
			// 
			this.scriptBottom.AutoSize = true;
			this.scriptBottom.Location = new System.Drawing.Point(155, 94);
			this.scriptBottom.Name = "scriptBottom";
			this.scriptBottom.Size = new System.Drawing.Size(58, 17);
			this.scriptBottom.TabIndex = 6;
			this.scriptBottom.TabStop = true;
			this.scriptBottom.Text = "&Bottom";
			this.scriptBottom.UseVisualStyleBackColor = true;
			// 
			// scriptLeft
			// 
			this.scriptLeft.AutoSize = true;
			this.scriptLeft.Location = new System.Drawing.Point(214, 94);
			this.scriptLeft.Name = "scriptLeft";
			this.scriptLeft.Size = new System.Drawing.Size(43, 17);
			this.scriptLeft.TabIndex = 7;
			this.scriptLeft.TabStop = true;
			this.scriptLeft.Text = "Le&ft";
			this.scriptLeft.UseVisualStyleBackColor = true;
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(260, 96);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(90, 13);
			this.label16.TabIndex = 8;
			this.label16.Text = "of Presenter View";
			// 
			// Options
			// 
			this.AcceptButton = this.ok;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.cancel;
			this.ClientSize = new System.Drawing.Size(444, 369);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.cancel);
			this.Controls.Add(this.ok);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "Options";
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
			this.Text = "Options";
			this.Load += new System.EventHandler(this.Options_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label fontDescription;
		private System.Windows.Forms.Button changeFont;
		private System.Windows.Forms.Button ok;
		private System.Windows.Forms.Button cancel;
		public System.Windows.Forms.ComboBox thumbSize;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private ShortcutKeyControl.ShortcutKeyControl nextSlide;
		private System.Windows.Forms.Label label9;
		private ShortcutKeyControl.ShortcutKeyControl firstSlideAlt;
		private System.Windows.Forms.Label label10;
		private ShortcutKeyControl.ShortcutKeyControl firstSlide;
		private System.Windows.Forms.Label label7;
		private ShortcutKeyControl.ShortcutKeyControl toggleScriptAlt;
		private System.Windows.Forms.Label label8;
		private ShortcutKeyControl.ShortcutKeyControl toggleScript;
		private System.Windows.Forms.Label label5;
		private ShortcutKeyControl.ShortcutKeyControl prevSlideAlt;
		private System.Windows.Forms.Label label6;
		private ShortcutKeyControl.ShortcutKeyControl prevSlide;
		private System.Windows.Forms.Label label4;
		private ShortcutKeyControl.ShortcutKeyControl nextSlideAlt;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label13;
		private ShortcutKeyControl.ShortcutKeyControl endSlideshowAlt;
		private System.Windows.Forms.Label label14;
		private ShortcutKeyControl.ShortcutKeyControl endSlideshow;
		private System.Windows.Forms.Label label11;
		private ShortcutKeyControl.ShortcutKeyControl lastSlideAlt;
		private System.Windows.Forms.Label label12;
		private ShortcutKeyControl.ShortcutKeyControl lastSlide;
		private System.Windows.Forms.RadioButton scriptBottom;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.RadioButton scriptLeft;
	}
}