﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;

namespace HovisPresent
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] argv) {
			if (argv.Length > 0 && argv[0] == "-console") {
				Win32Stuff.AllocConsole();
				Debug.Listeners.Add(new TextWriterTraceListener(Console.Out));
			}

			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new OpeningForm());
		}
	}
}
