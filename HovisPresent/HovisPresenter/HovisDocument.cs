﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace HovisPresent
{
	[Serializable]
	public class HovisDocument
	{
		public List<Slide> Slides { get; set; }

		[NonSerialized]
		private bool modified = false;
		public bool Modified { get { return modified; } set { modified = value; } }

		[NonSerialized]
		private string filename;
		public string Filename { get { return filename; } set { filename = value; directory = Path.GetDirectoryName(filename); } }

		[NonSerialized]
		private string directory;
		public string Directory { get { return directory; } }

		public HovisDocument() {
			Slides = new List<Slide>();
		}

		public void DebugDump() {
			Debug.WriteLine("------------- Slide List");
			int i=1;
			foreach (Slide s in Slides) {
				Debug.WriteLine("" + (i++) + " " + s.Filename);
			}
		}
	}
}
