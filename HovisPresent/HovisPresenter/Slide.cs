﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace HovisPresent
{
	[Serializable]
	public class Slide
	{
		public string Filename { get; set; }
		public string Script { get; set; }
		public bool Pause { get; set; }
		public string IntroScript { get; set; }

		public Slide() {
			Pause = true;
			Filename = Script = IntroScript = "";
		}

		public Slide(string slidefilename) {
			Filename = slidefilename;
			Script = "";
		}

		[OnDeserialized()]
		internal void OnDeserializedMethod(StreamingContext context) {
			if (IntroScript == null)
				IntroScript = "";
		}  

		/// <summary>
		/// Determines whether this slide is an image.
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if this slide is an image; otherwise, <c>false</c>.
		/// </returns>
		public bool IsImage {
			get {
				return IsFilenameAnImage(Filename);
			}
		}

		/// <summary>
		/// Determines whether the specified filename is an image.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <returns>
		/// 	<c>true</c> if the specified filename is an image; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsFilenameAnImage(string filename) {
			string ext = Path.GetExtension(filename);
			if (String.Compare(ext, ".jpg", true) == 0 ||
				String.Compare(ext, ".jpeg", true) == 0 ||
				String.Compare(ext, ".png", true) == 0 ||
				String.Compare(ext, ".gif", true) == 0 ||
				String.Compare(ext, ".bmp", true) == 0)
				return true;
			return false;
		}

		// return the image, or if a vid, the preview image (for preview next slide)
		public string PreviewImageFilename {
			get {
				if (IsImage)
					return Filename;

				return "previews\\" + Path.GetFileNameWithoutExtension(Filename) + ".png";
			}
		}

	}
}
