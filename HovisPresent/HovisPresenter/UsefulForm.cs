﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace HovisPresent
{
	/// <summary>
	/// It's like a form, just with a bunch of stuff I end up having to write over and over and over and over again added
	/// </summary>
	public class UsefulForm : Form
	{
		/// <summary>
		/// Enables/disables a list of controls.
		/// </summary>
		/// <param name="list">The list of controls</param>
		/// <param name="enabled">if set to <c>true</c> [enabled].</param>
		public void EnableControls(IEnumerable<Control> list, bool enabled) {
			foreach (Control c in list)
				c.Enabled = enabled;
		}

		/// <summary>
		/// Gets a string list of all the items in the list box
		/// </summary>
		/// <param name="lb">The lb.</param>
		/// <returns></returns>
		public static List<String> GetListBoxItems(ListBox lb) {
			List<string> list = new List<string>(lb.Items.Count);
			foreach (object o in lb.Items)
				list.Add(o.ToString());
			return list;
		}

		/// <summary>
		/// Puts the list of items into the list box
		/// </summary>
		/// <param name="lb">The lb.</param>
		/// <param name="items">The items.</param>
		public static void PutListBoxItems(ListBox lb, IEnumerable<string> items) {
			foreach (String s in items)
				lb.Items.Add(s);
		}

	}
}
