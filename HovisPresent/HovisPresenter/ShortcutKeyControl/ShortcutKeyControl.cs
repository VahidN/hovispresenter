﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;

namespace HovisPresent.ShortcutKeyControl
{
	public partial class ShortcutKeyControl : TextBox
	{
		private Keys key;

		[Browsable(true)]
		public Keys Key { get {return key;}  set { SetKey(value); } }

		public ShortcutKeyControl() {
			InitializeComponent();
		}

		private void SetKey(Keys value) {
			Keys button = value & Keys.KeyCode;
			Keys modifiers = value & Keys.Modifiers;

			// only a modifier key
			if (button == Keys.ControlKey || button == Keys.ShiftKey || button == Keys.Menu)
				return;
			
			// process modifiers
			List<string> bits = new List<string>();
			if ( (modifiers & Keys.Control) == Keys.Control)
				bits.Add("CTRL");
			if ( (modifiers & Keys.Alt) == Keys.Alt)
				bits.Add("ALT");
			if ( (modifiers & Keys.Shift) == Keys.Shift)
				bits.Add("SHIFT");

			// now convert the key name to text
			switch (button) {
				case Keys.Space:
					bits.Add("Space");
					break;
				case Keys.Next:
					bits.Add("PageDown");
					break;
				default:
					bits.Add("" + button);
					break;
			}

			key = value;
			Text = String.Join(" + ", bits.ToArray());
			Select(100, 100);
		}

		private void ShortcutControl_KeyDown(object sender, KeyEventArgs e) {
			e.SuppressKeyPress = true;
			Key = e.KeyData;
			OnTextChanged(new EventArgs());
		}

		private void clearMenuItem_Click(object sender, EventArgs e) {
			Key = Keys.None;
		}
	}
}
