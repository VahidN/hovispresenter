﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace HovisPresent
{
	public partial class AboutBox : Form
	{
		public AboutBox() {
			InitializeComponent();
		}

		private void linky(object sender, LinkLabelLinkClickedEventArgs e) {
			LinkLabel l = (LinkLabel)sender;
			System.Diagnostics.ProcessStartInfo info = new System.Diagnostics.ProcessStartInfo(l.Text);
			info.UseShellExecute = true;
			info.Verb = "open";
			System.Diagnostics.Process.Start(info);
		}
	}
}
