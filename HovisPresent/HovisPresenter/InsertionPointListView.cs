﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace HovisPresent
{
	public partial class InsertionPointListView : ListView
	{
		public InsertionPointListView() {
			InitializeComponent();
			DoubleBuffered = true;
		}

		private int insertionPoint = -1;
		public int InsertionPoint {
			get { return insertionPoint; }
			set { SetInsertionPoint(value); }
		}

		private void SetInsertionPoint(int point) {
			insertionPoint = point;
			InsertionMark.Index = point;

			if (SelectedItems.Count == 0)
				InsertionMark.AppearsAfterItem = false;
			else
				InsertionMark.AppearsAfterItem = point > SelectedItems[0].Index;
		}

	}
}
