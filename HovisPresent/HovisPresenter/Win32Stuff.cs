﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing;

namespace HovisPresent
{
	class Win32Stuff
	{
		[DllImport("kernel32.dll", SetLastError = true)]
		public static extern int AllocConsole();
		[DllImport("User32.dll")]
		public static extern bool ReleaseCapture();

		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern IntPtr FindWindow(string strClassName, string strWindowName);

		[DllImport("User32.dll")]
		public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
		public static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);
		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
		public static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, UInt32 wParam, UInt32 lParam);
		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
		public static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, UInt32 wParam, string lParam);

		[DllImport("user32.dll", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool SystemParametersInfo(uint uiAction, uint uiParam, uint pvParam, uint fWinIni);
		[DllImport("user32.dll", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool SystemParametersInfo(uint uiAction, uint uiParam, ref uint pvParam, uint fWinIni);

		[DllImport("user32.dll")]
		public static extern IntPtr SetFocus(IntPtr hWnd);

		[DllImport("user32.dll")]
		public static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

		[DllImport("user32.dll")]
		public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

		[DllImport("user32.dll")]
		public static extern bool GetWindowRect(IntPtr hWnd, out Rectangle lpRect);

		[DllImport("user32.dll")]
		public static extern bool InvalidateRect(IntPtr hWnd, IntPtr pRect, bool erase);

		[DllImport("user32.dll")]
		public static extern bool ScreenToClient(IntPtr hWnd, ref Rectangle rect );

		[DllImport("user32.dll")]
		public static extern bool EnumDisplayDevices(string lpDevice, uint iDevNum,
		   [In,Out] DISPLAY_DEVICE lpDisplayDevice, uint dwFlags);

		public const UInt32 WM_SETTEXT = 0x000C;
		public const UInt32 WM_SETREDRAW = 0x000B;
		public const UInt32 WM_MOVE = 0x0003;
		public const UInt32 WM_SIZE = 0x0005;
		public const int WM_NCLBUTTONDOWN = 0xA1;
		public const int WM_VSCROLL = 0x0115;
	
		public const int SB_LINEUP = 0;
		public const int SB_LINEDOWN = 1;

		public const int HTCAPTION = 0x2;

		public const UInt32 SPI_GETLOWPOWERACTIVE = 0x0053;
		public const UInt32 SPI_GETPOWEROFFACTIVE = 0x0054;
		public const UInt32 SPI_SETLOWPOWERACTIVE = 0x0055;
		public const UInt32 SPI_SETPOWEROFFACTIVE = 0x0056;
		public const UInt32 SPI_GETSCREENSAVEACTIVE = 0x0010;
		public const UInt32 SPI_SETSCREENSAVEACTIVE = 0x0011;

		public static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
		public static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);
		public static readonly IntPtr HWND_TOP = new IntPtr(0);

		public const UInt32 SWP_NOSIZE = 0x0001;
		public const UInt32 SWP_NOMOVE = 0x0002;
		public const UInt32 SWP_NOZORDER = 0x0004;
		public const UInt32 SWP_NOREDRAW = 0x0008;
		public const UInt32 SWP_NOACTIVATE = 0x0010;
		public const UInt32 SWP_FRAMECHANGED = 0x0020;  
		public const UInt32 SWP_SHOWWINDOW = 0x0040;
		public const UInt32 SWP_HIDEWINDOW = 0x0080;
		public const UInt32 SWP_NOCOPYBITS = 0x0100;
		public const UInt32 SWP_NOOWNERZORDER = 0x0200;  
		public const UInt32 SWP_NOSENDCHANGING = 0x0400;

		public const UInt32 DISPLAY_DEVICE_ATTACHED_TO_DESKTOP =0x00000001;
		public const UInt32 DISPLAY_DEVICE_MULTI_DRIVER        =0x00000002;
		public const UInt32 DISPLAY_DEVICE_PRIMARY_DEVICE      =0x00000004;
		public const UInt32 DISPLAY_DEVICE_MIRRORING_DRIVER    =0x00000008;
		public const UInt32 DISPLAY_DEVICE_VGA_COMPATIBLE      =0x00000010;
		public const UInt32 DISPLAY_DEVICE_REMOVABLE           =0x00000020;
		public const UInt32 DISPLAY_DEVICE_MODESPRUNED         =0x08000000;
		public const UInt32 DISPLAY_DEVICE_REMOTE              =0x04000000;
		public const UInt32 DISPLAY_DEVICE_DISCONNECT          =0x02000000;
		public const UInt32 DISPLAY_DEVICE_TS_COMPATIBLE       =0x00200000;
		public const UInt32 DISPLAY_DEVICE_UNSAFE_MODES_ON     =0x00080000;
		public const UInt32 DISPLAY_DEVICE_ACTIVE              =0x00000001;
		public const UInt32 DISPLAY_DEVICE_ATTACHED            =0x00000002;


		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
		public class DISPLAY_DEVICE
		{
			public int cb = 0;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
			public string DeviceName = new String(' ', 32);
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
			public string DeviceString = new String(' ', 128);
			public int StateFlags = 0;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
			public string DeviceID = new String(' ', 128);
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
			public string DeviceKey = new String(' ', 128);
		}

		public static UInt32 MAKELONG(UInt32 low, UInt32 high) {
			return low | (high << 16);
		}
	}
}
